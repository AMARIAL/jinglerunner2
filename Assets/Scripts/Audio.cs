using UnityEngine;
public enum Sounds
{
    crowds,
    door,
    jump,
    hit,
    spring,
    teleport,
    checkpoint,
    gravity,
    take,
    button,
    hitpoint
}
public enum Music
{
    menu,
    game
}
public class Audio : MonoBehaviour
{
    public static Audio ST  {get; private set;} // Audio.ST (Singltone)
    
    public bool MusicOn = true;
    public bool SoundOn = true;
    
    private AudioSource musicAudio;
    
    [SerializeField] private AudioSource crowds;
    [SerializeField] private AudioSource door;
    [SerializeField] private AudioSource jump;
    [SerializeField] private AudioSource hit;
    [SerializeField] private AudioSource spring;
    [SerializeField] private AudioSource teleport;
    [SerializeField] private AudioSource checkpoint;
    [SerializeField] private AudioSource gravity;
    [SerializeField] private AudioSource take;
    [SerializeField] private AudioSource button;
    [SerializeField] private AudioSource hitpoint;
    
    [SerializeField] private AudioClip menu;
    [SerializeField] private AudioClip game;
    
    private void Awake()
    {
        ST = this;
        musicAudio = GetComponent<AudioSource>();
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("MUSIC") && PlayerPrefs.GetString("MUSIC") == "OFF")
            MusicOn = false;
        if (PlayerPrefs.HasKey("SOUND") && PlayerPrefs.GetString("SOUND") == "OFF")
            SoundOn = false;
        
        if (!MusicOn)
        {
            musicAudio.Stop();
        }
    }
    
    public void SoundOnOff(bool isOn = true)
    {
        SoundOn = isOn;
        PlayerPrefs.SetString("SOUND", SoundOn ? "ON" : "OFF");
        PlayerPrefs.Save();
    }
    
    public void MusicOnOff(bool isOn = true)
    {
        MusicOn = isOn;
        
        PlayerPrefs.SetString("MUSIC", MusicOn ? "ON" : "OFF");
        PlayerPrefs.Save();
        
        if (MusicOn)
            musicAudio.Play();
        else
            musicAudio.Stop();
    }
    public void PlayMusic(Music music)
    {
        switch (music)
        {
            case Music.menu: musicAudio.clip = menu; break;
            case Music.game: musicAudio.clip = game; break;
        }
        if(MusicOn)
            musicAudio.Play();
    }
    public void PlaySound(Sounds sound)
    {
        if (!SoundOn) 
            return;
        
        switch (sound)
        {
            case Sounds.crowds: crowds.Play(); break;
            case Sounds.door: door.Play(); break;
            case Sounds.jump: jump.Play(); break;
            case Sounds.hit: hit.Play(); break;
            case Sounds.spring: spring.Play(); break;
            case Sounds.teleport: teleport.Play(); break;
            case Sounds.checkpoint: checkpoint.Play(); break;
            case Sounds.gravity: gravity.Play(); break;
            case Sounds.take: take.Play(); break;
            case Sounds.button: button.Play(); break;
            case Sounds.hitpoint: hitpoint.Play(); break;
        }
    }
}
