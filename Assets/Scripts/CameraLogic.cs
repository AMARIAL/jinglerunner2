﻿using UnityEngine;

public class CameraLogic : MonoBehaviour
{
    public static CameraLogic ST {get; private set;} // CameraLogic.ST (SinglTone)
    
    [SerializeField] private float dumping = 5.0f;
    [SerializeField] private float sizeDumping = 5000.0f;
    [SerializeField] private Vector2 offsetH = new Vector2(4.0f, 0.0f);
    [SerializeField] private Vector2 offsetV = new Vector2(2.0f, 3.0f);
    [SerializeField] private Vector2 offset;
    [SerializeField] private float[] sizeMinMax = new float[2] {8, 12};
    private Camera _camera;

    private Vector2 _position;
    private Vector2 _offsetPoint;

    private void Awake()
    {
        ST = this;
    }

    private void Start()
    {
        _camera = GetComponent<Camera>();
        offset = offsetH;
    }
    
    private void Update()
    {
        _position = CameraTarget.ST.targetPoint;

        _offsetPoint = new Vector2(_position.x + offset.x, _position.y + offset.y);
        
        //if (_camera.orthographicSize <= sizeMinMax[0])
        _camera.orthographicSize = Player.ST.direction == Direction.Down || Player.ST.direction == Direction.Up
            ? Mathf.Lerp(_camera.orthographicSize, sizeMinMax[0], sizeDumping * Time.deltaTime)
            : Mathf.Lerp(_camera.orthographicSize, sizeMinMax[1], sizeDumping * Time.deltaTime);

        transform.position = Vector2.Lerp(transform.position,_offsetPoint,dumping * Time.deltaTime);
    }

    public void ChangeOffsetPoint()
    {
        if (!Player.ST.isFlip)
        {
            offset = Player.ST.direction switch
            {
                Direction.Right => new Vector2(offsetH.x, offsetH.y),
                Direction.Down => new Vector2(offsetH.x - offsetV.x, -offsetH.y - offsetV.y),
                Direction.Left => new Vector2(-offsetH.x, -offsetH.y),
                Direction.Up => new Vector2(-offsetH.x + offsetV.x, offsetH.y + offsetV.y),
                _ => offset
            };
        }
        else
        {
            offset = Player.ST.direction switch
            {
                Direction.Left => new Vector2(-offsetH.x, offsetH.y),
                Direction.Up => new Vector2(offsetH.x - offsetV.x, offsetH.y + offsetV.y),
                Direction.Right => new Vector2(offsetH.x, -offsetH.y),
                Direction.Down => new Vector2(-offsetH.x + offsetV.x, -offsetH.y - offsetV.y),
                _ => offset
            };
        }
    }
    
}
