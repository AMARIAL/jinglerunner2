using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public static CameraTarget ST {get; private set;} // CameraTarget.ST (SinglTone)

    public Vector2 targetPoint;
    private Vector2 _direction;
    private RaycastHit2D _hit;
    [SerializeField] private float rayRange = 3.0f;
    [SerializeField] private LayerMask layerMaskGround;
    
    private void Awake()
    {
        ST = this;
    }

    private void Start()
    {
        targetPoint = transform.position;
    }

    private void Update()
    {
        _hit = Physics2D.Raycast(transform.position, _direction, rayRange, layerMaskGround);
        //Debug.DrawRay(transform.position, _direction*rayRange, Color.yellow);

        if (_hit.collider)
            targetPoint = _hit.point;
        else
        {
            var pos = Player.ST.transform.position;
            targetPoint = new Vector2 (pos.x, pos.y) + _direction * rayRange;
        }
            
    }

    public void ChangeTarget ()
    {
        if (!Player.ST.isFlip)
        {
            _direction = Player.ST.direction switch
            {
                Direction.Right => Vector2.down,
                Direction.Up => Vector2.right,
                Direction.Left => Vector2.up,
                Direction.Down => Vector2.left,
                _ => _direction
            };
        }
        else
        {
            _direction = Player.ST.direction switch
            {
                Direction.Right => Vector2.up,
                Direction.Up => Vector2.left,
                Direction.Left => Vector2.down,
                Direction.Down => Vector2.right,
                _ => _direction
            };
        }
    }
}
