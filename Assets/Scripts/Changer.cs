using UnityEngine;

public class Changer : MonoBehaviour
{

    public Direction direction = Direction.Up;

    private void Start()
    {
        transform.rotation = direction switch
        {
            Direction.Up => Quaternion.Euler(0, 0, 180),
            Direction.Left => Quaternion.Euler(0, 0, 270),
            Direction.Right => Quaternion.Euler(0, 0, 90),
            _ => Quaternion.Euler(0, 0, 0)
        };
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) 
            return;
        
        if(Player.ST.direction != direction)
            Player.ST.changeDir.Add(direction);
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && Player.ST.changeDir.Count > 0)
        {
            Player.ST.Hit();
        }
    }
}
