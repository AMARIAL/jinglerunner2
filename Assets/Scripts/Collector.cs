using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Collector : MonoBehaviour
{
    public static Collector ST {get; private set;} // Collector.ST (SinglTone)

    [SerializeField] private int hitPoints = 3;
    [SerializeField] public int giftCount = 0;
    private Image[] img;


    private void Awake()
    {
        ST = this;
    }
    
    private void Start()
    {
        hitPoints = 3;
        img = GameObject.Find("HitPoints").GetComponentsInChildren<Image>();
    }
    
    public void Hit()
    {
        Audio.ST.PlaySound(Sounds.hit);
        img[hitPoints-1].color = Color.black;
        hitPoints--;
        if(hitPoints <= 0)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    private void TakeBall()
    {
        Audio.ST.PlaySound(Sounds.hitpoint);
        if (hitPoints >= 3) 
            return;
        img[hitPoints].color = Color.white;
        hitPoints++;
    }
    
    private void TakeGift()
    {
        Audio.ST.PlaySound(Sounds.take);
        giftCount++;
        GameManager.ST.giftText.text = giftCount + " / " + GameManager.ST.giftMaxCount;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Item")) 
            return;
        
        var obj = other.gameObject;
        
        if (GameManager.ST.giftContainer.ContainsKey(obj))
        {
            TakeGift();
            GameManager.ST.giftContainer.Remove(obj);
            Destroy(obj);
        }
        else if (GameManager.ST.ballContainer.ContainsKey(obj))
        {
            TakeBall();
            GameManager.ST.giftContainer.Remove(obj);
            Destroy(obj);
        }
    }
}
