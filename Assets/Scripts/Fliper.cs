using UnityEngine;


public class Fliper : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) 
            return;
        
        Player.ST.Flip();
    }
    
    
}
