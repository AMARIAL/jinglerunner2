using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Direction: byte {None, Left, Right, Up, Down}
public enum GameState: byte {Menu, Levels, Game, Pause, GameOver}

public class GameManager : MonoBehaviour
{
	public static GameManager ST {get; private set;} // GameManager.ST (SinglTone)
	public GameState currentGameState;
	public int levelNumber = 0;
	
	public Dictionary<GameObject, Gift> giftContainer;
	public Dictionary<GameObject, ChristmasBall> ballContainer;
	
	private Button[] buttons;
	private List<string> levels;
	[SerializeField] private GameObject menuGrid; 
	
	[HideInInspector] public Text giftText;
	public int giftMaxCount = 0;

	[SerializeField] private Transform winPanel;
	[SerializeField] private Text giftWinCount;
	
	[SerializeField] private GameObject musicON;
	[SerializeField] private GameObject musicOFF;
	[SerializeField] private GameObject soundON;
	[SerializeField] private GameObject soundOFF;
	
	private void Awake()
    {
	    //Cursor.visible = false;
        ST = this;
        Application.targetFrameRate = 25;
        
        Time.timeScale = 1;
        
        giftContainer = new Dictionary<GameObject, Gift>();
        ballContainer = new Dictionary<GameObject, ChristmasBall>();

        levels = PlayerPrefs.GetString("LEVELS","").Split(',').ToList(); //   0/20,20/20,5/20
        
    }

	private void Start()
	{
		if (SceneManager.GetActiveScene().name.Contains('-') && 
		    int.TryParse(SceneManager.GetActiveScene().name.Split('-')[1], out levelNumber))
		{
			currentGameState = GameState.Game;
			giftText = GameObject.Find("GiftCount").GetComponent<Text>();
			giftMaxCount = FindObjectsOfType(typeof(Gift)).Length;
			giftText.text = "0 / " + giftMaxCount;
			Audio.ST.PlayMusic(Music.game);
		}
		else
		{
			if (PlayerPrefs.HasKey("MUSIC") && PlayerPrefs.GetString("MUSIC") == "OFF")
			{
				musicON.SetActive(false);
				musicOFF.SetActive(true);
			}
			
			if (PlayerPrefs.HasKey("SOUND") && PlayerPrefs.GetString("SOUND") == "OFF")
			{
				soundON.SetActive(false);
				soundOFF.SetActive(true);
			}
			
			if (levels.Count >= 1 && levels[0].Contains("/"))
			{
				buttons = menuGrid.GetComponentsInChildren<Button>();
				
				for (int i = 0; i < levels.Count; i++)
				{
					if(buttons[i+1].GetComponentsInChildren<Text>()[0].text != "-")
						buttons[i+1].interactable = true;
					buttons[i].GetComponentsInChildren<Text>()[1].text = levels[i];
					buttons[i].GetComponentsInChildren<Image>()[1].color = new Color(1.0f,1.0f,1.0f,1.0f);
				}
			}
			
			Audio.ST.PlayMusic(Music.menu);

		}
	}
	
	public void Win()
	{
		Audio.ST.PlaySound(Sounds.crowds);
		Time.timeScale = 0;
		
		var record = 0;

		try
		{
			record = Convert.ToInt32(levels[levelNumber - 1].Split('/')[0]);
		}
		catch (Exception e)
		{
			Debug.Log(e);
		}
		
		if (levelNumber <= levels.Count && Collector.ST.giftCount > record || levelNumber > levels.Count)
		{
			if(levelNumber-1 < levels.Count)
				levels[levelNumber-1] = Collector.ST.giftCount + "/" + giftMaxCount;
			else
				levels.Add(Collector.ST.giftCount + "/" + giftMaxCount);
			PlayerPrefs.SetString("LEVELS", string.Join("," , levels.ToArray()));
			PlayerPrefs.Save();
		}

		giftWinCount.text = Collector.ST.giftCount + " / " + giftMaxCount;
		winPanel.gameObject.SetActive(true);
	}
    
    public static IEnumerator Coroutine (string title)
    {
	    switch (title)
	    {
		    case "Start":
			    yield return new WaitForSeconds(2);
			    Player.ST.isMove = true;
			    Player.ST.sprite.enabled = true;
			    break;
	    }
	    yield break;
    }
}
