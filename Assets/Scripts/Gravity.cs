using UnityEngine;

public class Gravity : MonoBehaviour
{
    public static Gravity ST {get; private set;} // Gravity.ST (SinglTone)

    private Vector2 vertical, horizontal;
    private float force = 9.8f;
    [SerializeField] public Direction direction;

    private void Awake()
    {
        ST = this;
    }

    private void Start()
    {
        force = 9.8f;
        vertical = new Vector2(0,-force);
        horizontal = new Vector2(force,0);
        direction = Direction.Down;
    }

    public void ChangeDirection(Direction dir)
    {
        direction = dir;
        Audio.ST.PlaySound(Sounds.gravity);
        switch (dir)
        {
            case Direction.Left: Physics2D.gravity = horizontal;
                Player.ST.rig.gravityScale = -1;
                break;
            case Direction.Right: Physics2D.gravity = horizontal;
                Player.ST.rig.gravityScale = 1;
                break;
            case Direction.Up: 
                Physics2D.gravity = vertical;
                Player.ST.rig.gravityScale = -1;
                break;
            case Direction.Down: Physics2D.gravity = vertical;
                Player.ST.rig.gravityScale = 1;
                break;
        }
        
    }
}
