using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public static Menu St  {get; private set;} // Menu.St (Singltone)

    private void Awake()
    {
        St = this;
    }
    
    public void SoundOnOff(bool isOn = true)
    {
        Audio.ST.SoundOnOff(isOn);
    }
    
    public void MusicOnOff(bool isOn = true)
    {
        Audio.ST.MusicOnOff(isOn);
    }

    public void PlayButtonSound()
    {
        Audio.ST.PlaySound(Sounds.button);
    }
    
    public void ChangeLevel(int num)
    {
        SceneManager.LoadScene("Scenes/level-" + num);
    }
    
    public void NextLevel()
    {
        var next = GameManager.ST.levelNumber + 1;
        SceneManager.LoadScene("Scenes/level-" + next);
    }
    
    public void TryAgain()
    {
        Time.timeScale = 1;
        Player.ST.ChangeDirection(Direction.Right);
        SceneManager.LoadScene("Scenes/" + SceneManager.GetActiveScene().name);
    }
    
    public void PauseGame(bool flag = true)
    {
        Time.timeScale = flag ? 0 : 1;
    }
    
    public void QuitToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/menu");
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
    
}