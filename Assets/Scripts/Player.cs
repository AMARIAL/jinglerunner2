using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player ST {get; private set;} // Player.ST (SinglTone)
	
    [Header("Параметры")]
    [SerializeField] public float speed = 5.0f;
    [SerializeField] public bool isMove, isFlip;
    [SerializeField] public float jumpForce = 5f;
    [SerializeField] public List<Direction> changeDir;
    [SerializeField] public bool changeFlip = false;
    [SerializeField] public Direction direction;
    [SerializeField] public Vector2 jumpDir;
    
    [HideInInspector] public Rigidbody2D rig;
    [HideInInspector] public GroundDetection groundDetection;
    [HideInInspector] public Animator animator;
    [HideInInspector] public SpriteRenderer sprite;
    [SerializeField] public Vector2 respawnPosition;
    
    private static readonly int IsGrounded = Animator.StringToHash("isGrounded");

    private Vector2 _dir;
    private void Awake()
    {
        ST = this;
        rig = GetComponent<Rigidbody2D>();
        groundDetection = GameObject.Find("Legs").GetComponent<GroundDetection>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }
    
    private void Start()
    {
        jumpDir = Vector2.up;
        CameraTarget.ST.ChangeTarget();
        CameraLogic.ST.ChangeOffsetPoint();
        changeFlip = false;
        sprite.enabled = false;
        isMove = false;
        isFlip = false;
    }
    
    private void Update()
    {
        if(animator.GetBool(IsGrounded) != groundDetection.isGrounded)
            animator.SetBool(IsGrounded, groundDetection.isGrounded);
        
        if(isMove)
            Controls();
    }
    
    private void FixedUpdate()
    {
        if(isMove)
            Move(speed);
        
    }

    private void Controls()
    {
       if (Swipe.ST.swipeUp)
       {
           if (changeDir.Contains(Direction.Up)) 
               ChangeDirection(Direction.Up);
       }
       else if (Swipe.ST.swipeDown)
       {
           if (changeDir.Contains(Direction.Down)) 
               ChangeDirection(Direction.Down);
           else if (groundDetection.isGrounded)
               animator.Play("Tackle");
       }
       else if (Swipe.ST.swipeLeft)
       {
           if (changeDir.Contains(Direction.Left)) 
               ChangeDirection(Direction.Left);
       }
       else if (Swipe.ST.swipeRight)
       {
           if (changeDir.Contains(Direction.Right)) 
               ChangeDirection(Direction.Right);
       }
       else if (Swipe.ST.tap)
       {
           if (groundDetection.isGrounded)
           {
               Audio.ST.PlaySound(Sounds.jump);
               rig.AddForce(jumpDir * jumpForce, ForceMode2D.Impulse);
           }
               
       }
    }

    private void Move (float speed)
    {
        _dir = direction switch
        {
            Direction.Up => Vector2.up,
            Direction.Down => Vector2.down,
            Direction.Left => Vector2.left,
            Direction.Right => Vector2.right,
            _ => Vector2.zero
        };

        _dir *= speed;
        if(direction == Direction.Right || direction == Direction.Left)
            _dir.y = rig.velocity.y;
        else
            _dir.x = rig.velocity.x;
        rig.velocity = _dir;
    }

    public void ChangeDirection(Direction dir)
    {
        if (!isFlip)
        {
            switch (dir)
            {
                case Direction.Up:
                    Gravity.ST.ChangeDirection(Direction.Right);
                    jumpDir = Vector2.left;
                    transform.rotation = Quaternion.Euler(0,0,90);
                    break;
                case Direction.Down:
                    Gravity.ST.ChangeDirection(Direction.Left);
                    jumpDir = Vector2.right;
                    transform.rotation = Quaternion.Euler(0,0,270);
                    break;
                case Direction.Left:
                    Gravity.ST.ChangeDirection(Direction.Up);
                    jumpDir = Vector2.down;
                    transform.rotation = Quaternion.Euler(0,0,180);
                    break;
                case Direction.Right:
                    Gravity.ST.ChangeDirection(Direction.Down);
                    jumpDir = Vector2.up;
                    transform.rotation = Quaternion.Euler(0,0,0);
                    break;
                case Direction.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }
        else
        {
            switch (dir)
            {
                case Direction.Up:
                    Gravity.ST.ChangeDirection(Direction.Left);
                    jumpDir = Vector2.right;
                    transform.rotation = Quaternion.Euler(0,0,-90);
                    break;
                case Direction.Down:
                    Gravity.ST.ChangeDirection(Direction.Right);
                    jumpDir = Vector2.left;
                    transform.rotation = Quaternion.Euler(0,0,-270);
                    break;
                case Direction.Left:
                    Gravity.ST.ChangeDirection(Direction.Down);
                    jumpDir = Vector2.up;
                    transform.rotation = Quaternion.Euler(0,0,0);
                    break;
                case Direction.Right:
                    Gravity.ST.ChangeDirection(Direction.Up);
                    jumpDir = Vector2.down;
                    transform.rotation = Quaternion.Euler(0,0,180);
                    break;
                case Direction.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }
        
        direction = dir;
        changeDir.Clear();
        CameraTarget.ST.ChangeTarget();
        CameraLogic.ST.ChangeOffsetPoint();
        }

    public void Hit()
    {
        sprite.enabled = false;
        rig.velocity = Vector2.zero;
        isMove = false;
        transform.position = respawnPosition;
        Collector.ST.Hit();
        ChangeDirection(Direction.Right);
        StartCoroutine(GameManager.Coroutine("Start"));
    }

    public void Flip ()
    {
        isFlip = !isFlip;

        transform.localScale = isFlip ? new Vector2(-1, 1) : new Vector2(1, 1);
        
        switch (direction)
        {
            case Direction.Right:
                ChangeDirection(Direction.Left);
                break;
            case Direction.Left:
                ChangeDirection(Direction.Right);
                break;
            case Direction.Up:
                ChangeDirection(Direction.Down);
                break;
            case Direction.Down:
                ChangeDirection(Direction.Up);
                break;
            case Direction.None:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Ground")) 
            return;

        Hit();
    }
}
