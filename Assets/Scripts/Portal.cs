using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField] private bool isEnter;
    
    [SerializeField] private Transform exit;
    [SerializeField] public Direction changeDir = Direction.Right;
    [SerializeField] public Direction positionDir = Direction.Right;
    [SerializeField] public bool isFlip;

    private void Start()
    {
        if (gameObject.name == "Enter")
            isEnter = true;
        exit = transform.parent.Find("Exit").transform;
        changeDir = exit.GetComponent<Portal>().changeDir;
        
        transform.rotation = positionDir switch
        {
            Direction.Up => Quaternion.Euler(0, 0, 90),
            Direction.Down => Quaternion.Euler(0, 0, 90),
            _ => Quaternion.Euler(0, 0, 0)
        };
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player") || !isEnter) 
            return;
        
        Player.ST.transform.position = exit.position;

        if (!isFlip && Player.ST.isFlip || isFlip && !Player.ST.isFlip)
            Player.ST.Flip();
        
        if (Player.ST.direction != changeDir)
            Player.ST.ChangeDirection(changeDir);

        Player.ST.sprite.enabled = false;
        Player.ST.isMove = false;
        Player.ST.rig.velocity = Vector2.zero;
        
        StartCoroutine(GameManager.Coroutine("Start"));
        
        Audio.ST.PlaySound(Sounds.teleport);
    }

}
