using System;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) 
            return;

        Audio.ST.PlaySound(Sounds.checkpoint);
        _animator.Play("Fire");
        var position = transform.position;
        Player.ST.respawnPosition = new Vector2(position.x, position.y + 2.5f);
        
    }
}
