using UnityEngine;

public class Spring : MonoBehaviour
{

    [SerializeField] private Direction direction = Direction.Up;
    [SerializeField] private float force = 10.0f;
    private Animator _animator;
    private Vector2 _dir;
    
    private void Start()
    {
        _animator = GetComponent<Animator>();
        _dir = direction switch
        {
            Direction.Up => Vector2.up,
            Direction.Down => Vector2.down,
            Direction.Left => Vector2.left,
            Direction.Right => Vector2.right,
            _ => _dir
        };
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) 
            return;
        
        _animator.Play("Jump");
        Audio.ST.PlaySound(Sounds.spring);
        Player.ST.rig.velocity = Vector2.zero;
        Player.ST.rig.AddForce(_dir * force, ForceMode2D.Impulse);
    }
}
