using UnityEngine;

public class StartFinish : MonoBehaviour
{
    [SerializeField] private bool _isFinish;
    [SerializeField] private Transform _start;
    private Animator _animator;

    private void Start()
    {
        if (gameObject.name == "Finish")
            _isFinish = true;
        else
        {
            _start = GetComponent<Transform>();
            _animator = GetComponent<Animator>();
            Player.ST.transform.position = _start.position;
            Player.ST.respawnPosition = transform.position;
        }
        StartCoroutine(GameManager.Coroutine("Start"));
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) 
            return;
        
        if(!_isFinish) 
            Invoke(nameof(OpenDoor), 1.5f);
            
        else
        {
            GameManager.ST.Win();
        }
        
    }

    private void OpenDoor()
    {
        _animator.SetTrigger("Enter");
        Audio.ST.PlaySound(Sounds.door);
    }
    
}
