using System;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public static Timer ST {get; private set;} // Timer.ST (SinglTone)
    
    private Text _text;
    private float _timer;

    private void Awake()
    {
        ST = this;
    }

    private void Start()
    {
        _text = GetComponent<Text>();
        _timer = 0;
    }
    
    private void Update()
    {
        _timer += Time.deltaTime;
        _text.text = TimeSpan.FromSeconds(_timer).ToString(@"mm\:ss\:fff");
    }
}
